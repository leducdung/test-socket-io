import { useState } from "react";
import { Link, Outlet, Route, Routes, useParams } from "react-router-dom";
import io from "socket.io-client";
import "./App.css";
import Chat from "./Chat";
import { NoMatch } from "./no-match";
const socket = io.connect("http://localhost:3000");



function App() {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route index element={<Home />} />
        <Route path="about" element={<About />} />
        <Route path="*" element={<NoMatch />} />
        <Route path="/chat/:id" element={<AppChat />} />
      </Route>
    </Routes>
  );
}

function Layout() {
  return (
    <div>
      <h1>Welcome to the Home app!</h1>

      <p>
        This example demonstrates how you can stitch two React Router apps
        together using the <code>`basename`</code> prop on{" "}
        <code>`BrowserRouter`</code> and <code>`StaticRouter`</code>.
      </p>

      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/about">About</Link>
          </li>
          <li>
            {/* Use a normal <a> when linking to the "Inbox" app so the browser
                does a full document reload, which is what we want when exiting
                this app and entering another so we execute its entry point in
                inbox/main.jsx. */}
            <a href="/inbox">Inbox</a>
          </li>
          <li>
            <Link to="/chat/11133">chat</Link>
          </li>
        </ul>
      </nav>

      <hr />

      <Outlet />
    </div>
  );
}

function Home() {
  return (
    <div>
      <p>This is the home page.</p>
    </div>
  );
}

function About() {
  return (
    <div>
      <p>This is the about page.</p>
    </div>
  );
}

function AppChat() {
  const [username, setUsername] = useState("");
  const [username1, setUsername1] = useState("");
  const { id } = useParams();
  socket.emit("join_room", {
    token: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MjllZjdkM2YzNzRlZTFjOTBiNjkyYmEiLCJpYXQiOjE2NTQ2NTk2ODUsImV4cCI6MTY1NTI2NDQ4NX0.aPa5McjwuObjNihOZNR3GRQ7OWPLKd8ylChbp4X2fpk',
    roomId: id,
  });
  const joinRoom = () => {
    setUsername1(username)
  };

  return (
    <div className="App">
      <div className="joinChatContainer">
        <h3>Join A Chat</h3>
        <input
          type="text"
          placeholder="John..."
          onChange={(event) => {
            setUsername(event.target.value);
          }}
        />
        <button onClick={joinRoom}>Join A Room</button>
      </div>
      <Chat socket={socket} username={username1} room={id} />

    </div>
  );
}


export default App;
